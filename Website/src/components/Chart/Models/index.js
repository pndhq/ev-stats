import React from 'react'
import PropTypes from 'prop-types'
import { HorizontalBar } from 'react-chartjs-2'

import strings from 'config/strings'
import { DataShape } from 'config/types'
import { COLOR_RED } from 'config/colors'

import Toggle from 'components/Toggle'

import useHideNiche from '../useHideNiche'

import style from '../chart.module.css'

const Models = ({ data }) => {
  const [
    displayData,
    hideNiche,
    setHideNiche,
    cutOff,
  ] = useHideNiche(data)

  const labels = []
  const dataPoints = []

  displayData.forEach((item) => {
    labels.push(item.key)
    dataPoints.push(item.count)
  })

  return (
    <>
      <Toggle
        className={style.toggle}
        onChange={() => setHideNiche(!hideNiche)}
        label={strings.hideNiche(cutOff)}
        defaultChecked={hideNiche}
      />
      <HorizontalBar
        redraw
        height={16 * labels.length}
        data={{
          labels: labels,
          datasets: [{
            label: strings.countLabel,
            backgroundColor: COLOR_RED,
            data: dataPoints,
          }]
        }}
        legend={{ display: false }}
        options={{
          scales: {
            xAxes: [
              {
                ticks: {
                  min: 0,
                }
              }
            ],
            yAxes: [
              {
                gridLines: {
                  drawOnChartArea: false,
                }
              }
            ]
          }
        }}
      />
    </>
  )
}

export default Models

Models.propTypes = {
  data: PropTypes.arrayOf(DataShape).isRequired,
}
