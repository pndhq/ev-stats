import { useState } from 'react'

const useHideNiche = (data) => {
  const [ hideNiche, setHideNiche ] = useState(true)

  // calculate what should be the cut off value
  const sum = data.reduce((sum, { count }) => sum + parseInt(count), 0)
  const cutOff = Math.ceil(sum * 0.002)

  // filter data if necessary
  const displayData = hideNiche
    ? data.filter(({ count }) => count >= cutOff)
    : data
  
  return [
    displayData,
    hideNiche,
    setHideNiche,
    cutOff,
  ]
}

export default useHideNiche
