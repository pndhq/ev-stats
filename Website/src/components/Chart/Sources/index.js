import React from 'react'
import PropTypes from 'prop-types'
import { Pie } from 'react-chartjs-2'

import strings from 'config/strings'
import { DataShape } from 'config/types'
import { getColorForNumber } from 'config/colors'

const Sources = ({ data, size }) => {
  const labels = []
  const dataPoints = []
  const colors = []

  data.forEach((item, i) => {
    labels.push(item.key)
    dataPoints.push(item.count)
    colors.push(getColorForNumber(i))
  })

  return (
    <Pie
      height={size === 'half' ? 230 : null}
      data={{
        labels: labels,
        datasets: [{
          label: strings.countLabel,
          backgroundColor: colors,
          data: dataPoints,
        }]
      }}
    />
  )
}

export default Sources

Sources.propTypes = {
  data: PropTypes.arrayOf(DataShape).isRequired,
  size: PropTypes.oneOf(['full', 'half']),
}

Sources.defaultProps = {
  size: 'full',
}
