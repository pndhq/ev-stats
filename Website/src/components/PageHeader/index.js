import React from 'react'
import PropTypes from 'prop-types'

import Container from 'components/Container'

import style from './pageHeader.module.css'

const PageHeader = ({ updated }) => (
  <div className={style.header}>
    <Container className={style.container}>
      <div className={style.titles}>
        <h1>CEPiK EV Stats</h1>
        <p>Statystyki na temat pojazdów elektrycznych w Polsce na podstawie danych z Centralnej Ewidencji Pojazdów i Kierowców.</p>
        <p className={style.updated}>Ostatnia aktualizacja: {updated || '...'}</p>
      </div>
      <a
        href='https://www.facebook.com/groups/EVdriversPL/'
        title='Zapraszam do grupy Kierowcy samochodów elektrycznych w Polsce na Facebooku'
        target='_blank'
        className={style.image}
        rel='noopener noreferrer'
      >
        <img
          src={`${process.env.PUBLIC_URL}/ksewp_logo.png`}
          alt='Logo grupy Kierowcy samochodów elektrycznych w Polsce na Facebooku'
        />
      </a>
    </Container>
  </div>
)

export default PageHeader

PageHeader.propTypes = {
  updated: PropTypes.string,
}
