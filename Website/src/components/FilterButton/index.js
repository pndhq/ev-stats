import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import cx from 'lib/classNames'

import style from './filterButton.module.css'

const FilterButton = ({ children, to, className }) => (
  <NavLink
    to={to}
    className={cx(style.button, className)}
    activeClassName={style.active}
  >
    {children}
  </NavLink>
)

export default FilterButton

FilterButton.propTypes = {
  children: PropTypes.node,
  to: PropTypes.string.isRequired,
  className: PropTypes.string,
}
