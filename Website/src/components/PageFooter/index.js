import React from 'react'
import PropTypes from 'prop-types'

import Container from 'components/Container'
import ExtLink from 'components/ExtLink'

import style from './pageFooter.module.css'

const PageFooter = ({ updated }) => (
  <div className={style.footer}>
    <Container className={style.content}>
      <div className={style.column}>
        <dl>
          <dt>Autor:</dt>
          <dd>Michał Pałys-Dudek</dd>
          {/* eslint-disable-next-line */}
          <dd><a href='https://www.teslamodel3.pl' target='_blank' rel='noopener'>Tesla Model 3 w Polsce</a></dd>
          {/* eslint-disable-next-line */}
          <dd><a href='https://www.teslamodel3.pl/free' target='_blank' rel='noopener'>Link referencyjny do Tesli</a></dd>
          <dd><a href='mailto:michal@palys-dudek.pl?subject=CEPiK'>Kontakt</a></dd>
        </dl>
      </div>
      <div className={style.column}>
        <dl>
          <dt>Kod źródłowy:</dt>
          <dd><ExtLink to='https://gitlab.com/pndhq/ev-stats'>GitLab</ExtLink></dd>
          <dt>Wektorowa mapa Polski:</dt>
          <dd>
            Hiuppo / CC BY-SA<br />
            <ExtLink to='https://commons.wikimedia.org/wiki/File:POL_location_map.svg'>WikiMedia Commons</ExtLink>
          </dd>
        </dl>
      </div>
      <div className={style.column}>
        <p>
          Wszystkie dane pochodzą z <ExtLink to='http://www.cepik.gov.pl/'>Centralnej Ewidencji Pojazdów i Kierowców</ExtLink> (<ExtLink to='https://api.cepik.gov.pl/doc'>API</ExtLink>).
        </p>
        <p>
          Ostatnia aktualizacja: {updated || '...'}
        </p>
      </div>
    </Container>
  </div>
)

export default PageFooter

PageFooter.propTypes = {
  updated: PropTypes.string,
}
