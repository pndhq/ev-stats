import React from 'react'
import { NavLink } from 'react-router-dom'

import Container from 'components/Container'

import style from './pageNav.module.css'

export default () => (
  <div className={style.root}>
    <Container component='nav' noPadding className={style.nav}>
      <div className={style.tabs}>
        <NavLink to="/stats" className={style.tab} activeClassName={style.active}>Statystyki</NavLink>
        <NavLink to="/links" className={style.tab} activeClassName={style.active}>Linki</NavLink>
        <NavLink to="/about" className={style.tab} activeClassName={style.active}>Info</NavLink>
      </div>

      <div className={style.socials}>
        <div
          className='fb-share-button'
          data-href='https://evstats.pnd.io'
          data-layout='button'
          data-size='small'
        >
          <a
            target='_blank'
            href='https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fevstats.pnd.io&amp;src=sdkpreparse'
            className='fb-xfbml-parse-ignore'
            rel='noopener noreferrer'
          >Udostępnij</a>
        </div>
        <a
          className='twitter-share-button'
          href='https://twitter.com/intent/tweet?text=Aktualne dane o samochodach elektrycznych w Polsce'
        >Tweet</a>
        <script type="IN/Share" data-url="https://evstats.pnd.io"></script>
      </div>
    </Container>
  </div>
)
