export default (...classes) => {
  return classes.filter((cls) => !!cls).join(' ')
}
