import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'

import { DataSetInfoShape } from 'config/types'
import { vehicleTypes } from 'config/strings'

import Container from 'components/Container'
import Dataset from 'components/Dataset'
import FilterButton from 'components/FilterButton'

import style from './stats.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Stats = ({ datasets, error }) => {
  if (error) {
    return (
      <Container className={style.error}>
        <FontAwesomeIcon className={style.errorIcon} icon='exclamation-triangle' />
        <p>Ups, nie udało się załadować danych :(</p>
        <p>Prawdopodobnie coś poszło nie tak podczas ściągania danych z CEPiK.</p>
        <p>Zdarza się...</p>
        <p>Spróbuj wrócić za kilka dni po następnej automatycznej aktualizacji albo skontaktuj się z autorem, żeby uruchomił proces jeszcze raz.</p>
      </Container>
    )
  }

  const dateFilters = []
  const years = datasets.filter(({ key, type }) => type === 'date' && key.length === 4)
    .map((d) => d.key)
    .reverse()

  years.forEach((year) => {
    dateFilters.push({
      url: year,
      label: year,
    })
    // find all quarters for this year
    datasets.filter(({ key, type }) => type === 'date' && key.startsWith(year + 'Q'))
      .reverse()
      .forEach(({ key }) => {
        dateFilters.push({
          url: key,
          label: key.substr(4),
        })
      })
  })

  const filters = [
    {
      label: 'Wg typu',
      filters: [
        {
          url: 'all',
          label: vehicleTypes.all,
        },
        ...datasets.filter((d) => d.type === 'vehicleType').map(({ key }) => ({
          url: key,
          label: vehicleTypes[key],
        }))
      ],
    },
    {
      label: 'Wg daty',
      filters: [ ...dateFilters ],
    }
  ]

  return (
    <Container>
      <ul className={style.filtersWrap}>
        {filters.map(({ label, filters }) => (
          <li className={style.filtersRow} key={label}>
            <div className={style.filtersType}>{label}</div>
            <ul className={style.filters}>
              {filters.map(({ url, label }) => (
                <li className={style.filter} key={url}>
                  <FilterButton to={`/stats/${url}`}>
                    {label}
                  </FilterButton>
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
      <Switch>
        {datasets.map((dataset) => (
          <Route path={`/stats/${dataset.key}`} key={dataset.key}>
            <Dataset dataset={dataset} />
          </Route>
        ))}
        <Route path='/stats'>
          <Redirect to='/stats/all' />
        </Route>
      </Switch>
    </Container>
  )
}

export default Stats

Stats.propTypes = {
  datasets: PropTypes.arrayOf(DataSetInfoShape),
  error: PropTypes.bool,
}

Stats.defaultProps = {
  datasets: [],
  error: false,
}
