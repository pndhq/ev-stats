import React from 'react'

import Container from 'components/Container'
import ExtLink from 'components/ExtLink'

import style from 'styles/contentPage.module.css'

export default () => (
  <Container className={style.page}>
    <h2>Linki</h2>
    <p>Strony i miejsca w Internecie związane z samochodami elektrycznymi.</p>
    <p>Zapraszam do kontaktu jeśli coś jeszcze powinno się tu znaleźć. Póki co lista jest mocno subiektywna i dobrana na podstawie tego gdzie ja osobiście najczęściej zaglądam i z czego korzystam :)</p>

    <h3>Ta strona i źródło danych:</h3>
    <ul>
      <li>
        <ExtLink to='https://gitlab.com/pndhq/ev-stats'>Kod źródłowy strony w serwisie GitLab</ExtLink>
        <ul>
          <li><ExtLink to='https://gitlab.com/pndhq/ev-stats/pipelines'>GitLab CI Pipelines</ExtLink> - lista automatycznych zadań zbierających dane i aktualizujących stronę</li>
          <li><ExtLink to='https://gitlab.com/pndhq/ev-stats/-/issues'>Issues</ExtLink> - miejsce do zgłaszania uwag i sugestii odnośnie działania strony</li>
        </ul>
      </li>
      <li>
        <ExtLink to='http://www.cepik.gov.pl/'>Centralna Ewidencja Pojazdów i Kierowców</ExtLink>
        <ul>
          <li><ExtLink to='https://api.cepik.gov.pl/doc'>Publiczne API</ExtLink></li>
        </ul>
      </li>
    </ul>

    <h3>Społeczność:</h3>
    <ul>
      <li><ExtLink to='https://www.facebook.com/groups/EVdriversPL/'>Kierowcy samochodów elektrycznych w Polsce</ExtLink> - grupa na Facebooku</li>
    </ul>

    <h3>Serwisy informacyjne o samochodach elektrycznych:</h3>
    <ul>
      <li><ExtLink to='https://elektrowoz.pl/'>elektrowoz.pl</ExtLink></li>
      <li><ExtLink to='http://www.naprad.eu/'>NaPrad.eu</ExtLink></li>
      <li><ExtLink to='https://napradzie.pl/'>napradzie.pl</ExtLink></li>
    </ul>

    <h3>Aplikacje i sieci ładowania:</h3>
    <ul>
      <li><ExtLink to='https://www.plugshare.com/'>PlugShare</ExtLink> - mapa ładowarek na świecie i w Polsce</li>
      <li><ExtLink to='https://www.plugsurfing.com/'>plugsurfing</ExtLink> - aplikacja pozwalająca na korzystanie z ładowarek wielu różnych sieci korzystając z jednego klucza / karty / aplikacji</li>
      <li><ExtLink to='https://greenwaypolska.pl/'>GreenWay</ExtLink> - ogólnopolska sieć ładowania samochodów elektrycznych</li>
    </ul>
    
    <h3>Strony autora:</h3>
    <ul>
      <li><ExtLink to='https://www.youtube.com/channel/UCLJlXUKQjT7L1c_Dbngoe3g'>YouTube: Tesla Model 3 w Polsce</ExtLink></li>
      <li><ExtLink to='https://www.facebook.com/model3polska/'>Facebook: Tesla Model 3 w Polsce</ExtLink></li>
      <li><ExtLink to='https://www.instagram.com/model3pl/'>Instagram: @model3pl</ExtLink></li>
      <li><ExtLink to='https://www.teslamodel3.pl/free'>Link referencyjny do Tesli</ExtLink> - 1500 km ładowania za darmo przy zakupie Tesli z mojego linku</li>
    </ul>
  </Container>
)
