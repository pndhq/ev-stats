interface Meta {
  count: number,
  page: number,
  limit: number,
}

interface Links {
  first: string,
  last: string,
  next?: string,
  prev?: string,
  self: string,
}

interface CepikResponse {
  data: Array<Record<string, any>>,
  meta: Meta,
  links: Links,
}

export default CepikResponse
