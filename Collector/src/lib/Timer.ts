/*
 * Timer
 *
 * Just a simple Timer for profiling how long things take.
 *
 *    const timer = new Timer()
 *    console.log(`This took ${timer.stop()} ms`)
 *
 */
const now = (): number => (new Date()).getTime()

export default class Timer {

  readonly startTime: number = now()
  private endTime: number = 0
  private time: number = 0

  current () {
    // if already stopped then return that
    if (this.time) {
      return this.time
    }

    return now() - this.startTime
  }

  stop () {
    this.endTime = now()
    this.time = this.endTime - this.startTime
    return this.time
  }
}
