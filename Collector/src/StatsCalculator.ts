import { Writable } from 'stream'
import moment from 'moment'
import sortBy from 'lodash/sortBy'

import { isEmptyIndicator } from '@lib/helpers'
import {
  FORMAT_YEAR,
  FORMAT_QUARTER,
  FORMAT_MONTH, 
  FORMAT_CEPIK,
} from '@config'

import Car from './Car'

export default class StatsCalculator extends Writable
{
  private stats = {
    brands: {},
    models: {},
    versions: {},
    vehicleTypes: {},
    registrationsByYear: {},
    registrationsByQuarter: {},
    registrationsByMonth: {},
    sources: {},
    voivodeships: {},
  }

  readonly promise: Promise<Record<string, any>>

  constructor () {
    super({ objectMode: true })

    this.promise = new Promise((resolve, reject) => {
      this.on('finish', () => resolve(this.stats))
      this.on('error', reject)
    })
  }

  _write (car: Car, encoding: string, next: Function) {
    const registrationDate = moment(car.firstRegistrationDate, FORMAT_CEPIK)

    this.incrementStat(this.stats.brands, car.brand)
    this.incrementStat(this.stats.models, this.createName([car.brand, car.model]))
    this.incrementStat(this.stats.versions, this.createName([car.brand, car.model, car.type, car.version]))
    this.incrementStat(this.stats.vehicleTypes, car.vehicleType)
    this.incrementStat(this.stats.registrationsByYear, registrationDate.format(FORMAT_YEAR))
    this.incrementStat(this.stats.registrationsByQuarter, registrationDate.format(FORMAT_QUARTER))
    this.incrementStat(this.stats.registrationsByMonth, registrationDate.format(FORMAT_MONTH))
    this.incrementStat(this.stats.sources, car.source)
    
    if (car.voivodeship) {
      this.incrementStat(this.stats.voivodeships, car.voivodeship)
    }
    
    next()
  }

  getRawStats () {
    return this.stats
  }

  getStats (): Record<string, any> {
    return {
      brands: this.sortDesc(this.stats.brands),
      models: this.sortDesc(this.stats.models),
      versions: this.sortDesc(this.stats.versions),
      vehicleTypes: this.sortDesc(this.stats.vehicleTypes),
      registrationsByYear: this.sortTimeseries(this.stats.registrationsByYear, 'year', FORMAT_YEAR),
      registrationsByQuarter: this.sortTimeseries(this.stats.registrationsByQuarter, 'quarter', FORMAT_QUARTER),
      registrationsByMonth: this.sortTimeseries(this.stats.registrationsByMonth, 'month', FORMAT_MONTH),
      sources: this.sortDesc(this.stats.sources),
      voivodeships: this.sortDesc(this.stats.voivodeships),
    }
  }

  /**
   * Increments count on an entry in a stat map.
   *
   * @param stat Stat map.
   * @param value Specific entry name.
   */
  private incrementStat (stat: Record<string, number>, value: string) {
    if (stat[value] === undefined) {
      stat[value] = 0
    }

    stat[value]++
  }

  /**
   * Create a name from given parts but omits blanks and unknowns.
   *
   * @param parts Name parts.
   * @return string
   */
  private createName (parts: Array<string | undefined | null>): string {
    const name = parts.filter((s) => !isEmptyIndicator(s)).join(' ')
    return name.length ? name : 'N/A'
  }

  /**
   * Convert stats into a sorted array.
   *
   * @param data Stats map / dictionary.
   * @return Array
   */
  private sortDesc (data: Record<string, number>) {
    const items = Object.keys(data).map((key) => ({
      key,
      count: data[key]
    }))

    return sortBy(items, ['count']).reverse()
  }

  /**
   * Creates a timeseries, sorts by date and fills in the blanks.
   * 
   * @param data Stats map / dictionary.
   * @param step What time period step?
   * @param dateFormat Key format.
   * @return Array
   */
  private sortTimeseries (
    data: Record<string, number>,
    step: 'year' | 'quarter' | 'month',
    dateFormat: string
  ) {
    const periods = sortBy(Object.keys(data), (p) => p)
    const current = moment(periods[0], dateFormat)
    const now = moment()
    
    const items = []

    while (current.isBefore(now)) {
      const key = current.format(dateFormat)
      items.push({
        key,
        count: data[key] || 0,
      })
      current.add(1, step)
    }

    return items
  }
}
