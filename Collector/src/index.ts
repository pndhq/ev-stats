import moment from 'moment'
import minimist from 'minimist'
import 'module-alias/register'

import CsvWriter from '@lib/CsvWriter'
import { filterStream } from '@lib/FilteredStream'
import { writeJson, writeCsv } from '@lib/helpers'
import createConfig, { DataSetConfig } from '@config'

import cepik from './Cepik'
import CarsReader from './CarsReader'
import StatsCalculator from './StatsCalculator'

require('source-map-support').install()

// uh, super insecure but fuck it
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'

process.on('uncaughtException', (err) => {
  console.log('UNCAUGHT EXCEPTION')
  console.log(err.message)
  console.dir(err)
  process.exit(1)
})

process.on('unhandledRejection', (reason, promise) => {
  console.log('UNHANDLED REJECTION')
  console.log(reason)
  console.log(promise)
  process.exit(1)
})

const argv = minimist(process.argv.slice(2))
run(
  argv['output-dir'],
  argv.from,
  argv.step
).then(() => {
  console.log('All done.')
}, (e) => {
  console.error(e)
  process.exit(1)
})

async function run (
  outputDir = '../data/',
  startDate = '20100101',
  step: 'y' | 'Q' | 'M' | 'w' = 'y'
) {
  const config = createConfig(startDate)

  // fetch some required meta data from CEPiK
  console.log('Fetching metadata from CEPiK...')
  await cepik.fetchMetaData()

  // start fetching cars that we're interested in from CEPiK
  console.log(`Starting reading registration data from CEPiK since ${startDate}...`)
  const carsReader = new CarsReader(startDate, step)
  carsReader.setMaxListeners(config.datasets.length * 4) // there's gonna be a lot of streams attached to the reader

  // handle pre-defined datasets
  const datasets: Array<DataSetInfo> = config.datasets
    .map((dataset: DataSetConfig) => {
      const dataFile = `${dataset.key}/cepik-ev-${dataset.key}.csv`
      const csvWriter = new CsvWriter(outputDir + dataFile)
      const statsCalculator = new StatsCalculator()

      carsReader.pipe(filterStream(dataset.filter)).pipe(csvWriter)
      carsReader.pipe(filterStream(dataset.filter)).pipe(statsCalculator)

      const info: DataSetInfo = {
        key: dataset.key,
        type: dataset.type,
        dataFile: dataFile,
        promise: Promise.all([
          csvWriter.promise,
          statsCalculator.promise
        ]),
        stats: [],
      }

      // after stats have been calculated write the ones
      // that we're interested in to files
      // and attach info about them
      statsCalculator.promise.then(() => {
        const stats = statsCalculator.getStats()

        info.stats = dataset.stats.map((key) => {
          const file = `${dataset.key}/cepik-ev-${dataset.key}-${key}.csv`
          writeCsv(outputDir + file, stats[key])
          return { key, file }
        })
      })

      return info
    })

  // wait for all streams to finish their work
  await Promise.all([
    carsReader.promise,
    ...datasets.map((d) => d.promise),
  ])

  // write meta info to JSON file
  const metaFile = outputDir + 'meta.json'
  writeJson(metaFile, {
    date: moment().format('YYYY-MM-DD HH:mm'),
    datasets: datasets.map((dataset) => {
      const { promise, ...rest } = dataset

      // output some debug info
      console.log(`Saved dataset raw data in ${rest.dataFile}`)
      rest.stats.forEach((stat) => {
        console.log(` Saved ${stat.key} in ${stat.file}`)
      })
      
      return rest
    }),
    fuelTypes: cepik.getFuelTypes(),
  })

  // debug
  console.log(`Saved meta information to ${metaFile}`)
}

interface DataSetInfo {
  key: string
  type: 'noFilter' | 'vehicleType' | 'date'
  dataFile: string
  promise: Promise<any>
  stats: Array<DataSetStatInfo>
}

interface DataSetStatInfo {
  key: string
  file: string
}
